﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fluffy
{
    public partial class Form1 : Form
    {
        const int backspace = 8;
        const int LeftArrow = 37;
        const int RightArrow = 39;
        int Tab = 1;

        private string[] _matches = new[]
            {
                "Successful Industry (St Paul)",
                "Successful Industry (Paris)",
                "Successful Industry (London)",
                "Sucralose"
            };

        string StPaul = "Successful Industry (St Paul)";
        //int counter = 0;
        //int check = 0;

        private string _userInput = string.Empty;



        public Form1()
        {
            InitializeComponent();
        }



        public void myTextBox_TextChanged(object sender, EventArgs e)
        {
            //if ((myTextBox.Text[counter] == StPaul[counter]) && counter >= 2)
            //{
            //    myTextBox.Text = "Successful Industry (St Paul)";
            //    myTextBox.SelectionStart = counter + 1;
            //    myTextBox.SelectionLength = myTextBox.Text.Length;
            //    counter = counter + 1;
            //}

            //if ((myTextBox.Text == "suc" || myTextBox.Text == "Suc") && counter < 2)
            //{
            //    myTextBox.Text = "Successful Industry (St Paul)";
            //    myTextBox.SelectionStart = 3;

            //    myTextBox.SelectionLength = myTextBox.Text.Length;
            //    counter = counter + 2;
            //}
            //if (counter > 3)
            //{
            //    check = check + 1;
            //        if (check%2 > 0)
            //    {
            //        counter = counter -1;
            //    }
            //}

            //System.Diagnostics.Trace.WriteLine("Counter is at " + counter + " and " + StPaul[counter] + " and " + myTextBox.Text[counter]);
            //// MessageBox.Show("Counter is at " + counter + " and " + StPaul[counter] + " and " + textBox1.Text[counter]);
        }



        private void cancelButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void OnKeyPress(object sender, KeyPressEventArgs e)
        {


            TextBox box = (TextBox)sender;

            // Update the user input
            if (e.KeyChar == backspace)
            {
                if (_userInput.Length > 1)
                {
                    // TODO - handle the case when the insertion point is not at the end

                    // Remove the last character from the string
                    _userInput = _userInput.Substring(0, _userInput.Length - 1);
                }
                else
                {
                    _userInput = string.Empty;
                }
            }
            else
            {
                    // TODO - use box.SelectionStart and box.SelectionLength to determine proper place to insert/text to replace

                    _userInput += e.KeyChar;
            }

            // If we have a match, display the first one
            string newContent = _userInput;

            if (_userInput.Length >= 3)
            {
                for (int i = 0; i < _matches.Length; i++)
                {
                    if (_matches[i].StartsWith(_userInput, StringComparison.InvariantCultureIgnoreCase))
                    {
                        newContent = _matches[i];
                        break;
                    }
                }
            }

            // Update the form with the new stuff
            box.Text = newContent;
            box.Select(_userInput.Length, 0);       // TODO - needs to handle case when insertion point not at the end
            //myTextBox.Text.Length = _userInput.Length;
            otherTextBox.Text = _userInput;

            myTextBox.SelectionStart = _userInput.Length;
            myTextBox.SelectionLength = myTextBox.Text.Length;
            // We handled the event, tell the form not to handle it
            e.Handled = true;
        }



        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Tab)
            {
                // TODO - cycle through autocomplete alternatives - left as an exercise for the reader
                
                return true;
            }
            else if (keyData == Keys.Left)
            {
                return true;
            }
            else if (keyData == Keys.Right)
            {
                return true;
            }
            else if (keyData == Keys.End)
            {
                return true;
            }
            else if (keyData == Keys.Home)
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }



        // TODO - delete this!
        private void greetButton_Click(object sender, EventArgs e)
        {
            Animal animal = new Monkey();

            MessageBox.Show(this, animal.Greeting(), "A greeting from an animal");
        }




    }
}
